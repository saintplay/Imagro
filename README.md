# Imagro
Trabajo final de Matemática Discreta - UPC - ciclo 2015 - 01.

Carpeta De Recursos: http://1drv.ms/1RNrySX

Descripciòn:
  - Este programa escrito en c++, con el uso de Widows Forms, y librerias nativos de windows de procesamiento de imagen.
  - Sirve para mejor la calidad, iluminación, resolución, colorización de una imagen, dependiendo del tipo de problema que tenga esta imagen.
  
Integrantes:
- Manuel Figueroa
- Diego Jara
- Alondra Rosas
- Mario Vega
- Percy Briceño

Profesor:
- Daniel Muñoz
